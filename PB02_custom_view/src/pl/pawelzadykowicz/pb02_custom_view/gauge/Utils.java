package pl.pawelzadykowicz.pb02_custom_view.gauge;

public final class Utils {
	
	private Utils() {}
	
	/**
	 * znormalizowana maksymalna watość jaka może być reprezentowana przez wskaźnik.
	 */
	public static final int MAX_ROTATE_LEVEL = 10000;

	/**
	 * znormalizowan minimalna wartość jaka może być reprezentowana przez wskaźnik.
	 */
	public static final int MIN_ROTATE_LEVEL = 0;
	
	/**
	 * Kąt 45 stopni.
	 */
	public static final int DEGREE_45 = 45;
	
	/**
	 * Kąt 225 stopni. 270 to cała skala, początkowe przesunięcie wynosi 45.
	 */
	public static final int DEGREE_225 = 225;
	
	/**
	 * Kąt 270 stopni.
	 */
	public static final int DEGREE_270 = 270;

	/**
	 * 135 stopni - poczatek skali na tarczy wskaźnika.
	 */
	public static final float DEGREE_135 = 135;

	/**
	 * kąt pełny - przydatny, bo początek skali wskaźnika jest przesunięty o 135 stopni,
	 * względem zerowego kąta przy rysowaniu łuków.  - łatwiejsze odejmowanie kątów.
	 */
	public static final float DEGREE_360 = 360;

	/**
	 * normalizuje aktualną wartość w podanej skali. zwracamy wartość z zakresu 0 do 10000 proporcjonalną do
	 * argumentu value. gdy value = min zwracamy 0, gdy value = max zwracamy max.
	 * 
	 * @param value
	 *            wartość do znormalizowania
	 * @param min
	 *            minimalna wartość skali.
	 * @param max
	 *            maksymalna wartość skali.
	 * @return liczba z zakresu 0..10000 proporcjonalna do wartości.
	 */
	public static int normalize(final float value, final float min, final float max) {
		float range = max - min;
		if (range < 0) {
			throw new IllegalArgumentException("Niewłaściwy zakres (" + min + "," + max + "); max - min = : "
					+ range);
		}
		float valFactor = Math.abs(value - min) / range;
		int normalRange = MAX_ROTATE_LEVEL - MIN_ROTATE_LEVEL;
		float normalFactor = valFactor * normalRange;
		return (int) (normalFactor + MIN_ROTATE_LEVEL);
	}}
