package pl.pawelzadykowicz.pb02_custom_view.gauge;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnTouchListener;
import java.util.Locale;
import pl.pawelzadykowicz.pb02_custom_view.R;

public class Gauge extends View implements OnTouchListener  {

	private static final String TAG = Gauge.class.getName();
	/**
	 * obiekt mierzący prędkość przesuwania palca .po widoku.
	 */
	private VelocityTracker vt;
	/**
	 * odległość rysunku tarczy od krawędzi widoku.
	 */
	private int marginTop;
	/**
	 * odległość rysunku tarczy od krawędzi widoku.
	 */
	private int marginBottom;
	/**
	 * odległość rysunku tarczy od krawędzi widoku.
	 */
	private int marginLeft;
	/**
	 * odległość rysunku tarczy od krawędzi widoku.
	 */
	private int marginRight;
	/**
	 * grubość pędzla na tarczy wskaźnika (szeokość kolorowego paska).
	 */
	private float strokeWidth;
	/**
	 * grubość obramowania wokół kolorowego paska na tarczy wskaźnika.
	 */
	private float borderWidth;
	/**
	 * bitmapa tarczy wskaźnika - tworzona dynamicznie.
	 */
	private Bitmap bitmapShield;

	/**
	 * Kolor obramowania.
	 */
	private int borderColor = Color.parseColor("#1b272f");
	/**
	 * Kolor obszaru wartości mniejszych niż normalna.
	 */
	private int lzc = Color.YELLOW;

	/**
	 * Kolor obszaru wartości większych niż normalna.
	 */
	private int hzc = Color.RED;

	/**
	 * Kolor obszaru o normalnej wartości.
	 */
	private int nzc = Color.GREEN;

	/**
	 * Min wartość obszaru o normalnej wartości.
	 */
	private float ll = 300;
	/**
	 * Max wartość obszaru o normalnej wartości.
	 */
	private float hl = 2200;

	/**
	 * aktualna wartość reprezentowana przez wskaźnik.
	 */
	private float value = 0;
	/**
	 * zadeklarowana maksymalna wartość na wskaźniku.
	 */
	private float max = 3000;
	/**
	 * zadeklarowana minimalna wartość na wskaźniku.
	 */
	private float min = 0;

	/**
	 * Drawable reprezentujący wskazówkę na zegarze.
	 */
	private Drawable pointer;
	/**
	 * Bitmapa wskazówki.
	 */
	private Bitmap pointerBitmap;

	/**
	 * Przesunięcie dla wskazówki. Obejście bug'a, gdzie dla niższego API nie
	 * działa rotateDrawable przy współrzędnych innych niż zaczynających się od
	 * lewego gónego rogu.
	 */
	private float pointerTX;
	/**
	 * {@link ClockView#pointerTX}.
	 */
	private float pointerTY;

	/**
	 * znormalizowany stopień obrócenia wskazówki - może przyjmować wartości od
	 * 0 do 10000. wartość 10000 oznacza maksymalne wychylenie zdefiniowane w
	 * zasobie drawable/gauge_pointer_rotation. (zobacz
	 * {@link android.graphics.drawable.Drawable#setLevel(int)}.
	 */
	private int indicatorLevel;
	/**
	 * atrybuty tekstu.
	 */
	private Paint textPaint;

	public Gauge(Context context, AttributeSet attrs) {
		super(context, attrs);
		Log.d(TAG, "Gauge(Context, AttributeSet)");
		TypedArray a = null;
		try
		{
			a = context.obtainStyledAttributes(attrs, R.styleable.GaugesAttrs);
			lzc = a.getColor(R.styleable.GaugesAttrs_lzc, Color.TRANSPARENT);
			nzc = a.getColor(R.styleable.GaugesAttrs_nzc, Color.TRANSPARENT);
			hzc = a.getColor(R.styleable.GaugesAttrs_hzc, Color.TRANSPARENT);
			ll = a.getFloat(R.styleable.GaugesAttrs_ll, 0);
			hl = a.getFloat(R.styleable.GaugesAttrs_hl, 0);
		}
		finally
		{
			if (a != null)
			{
				a.recycle();
			}
		}
	}

	{
		pointer = getResources().getDrawable(R.drawable.gauge_stripe_pointer);
		pointerBitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.clock_pointer);
	}

	/*
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		setMeasuredDimension(400, 400); // TODO: uwzględnić rozmiar podany w layoucie
		int width = getWidth();
		int height = getHeight();
		Log.d(TAG, "onMeasure(); width: " + width + "; height: " + height);
	}
	*/

	@Override
	protected void onLayout(boolean changed, int left, int top, int right,
			int bottom) {
		super.onLayout(changed, left, top, right, bottom);
		int width = getWidth();
		int height = getHeight();
		Log.d(TAG, "onLayout(); changed: " + changed + "; width: " + width
				+ "; height: " + height);
		if (changed) {
			init();
		}

	}

	/**
	 * przygotowanie wyglądu widoku - rysowanie tła wskaźnika.
	 */
	private void init() {
		//
		marginTop = getMeasuredHeight() / 12;
		marginBottom = marginTop;
		// init() jest wołane z onLayout(), więc mamy tu getMeasuredWidth i
		// getMeasuredHeight()
		marginLeft = (getMeasuredWidth() - (getMeasuredHeight() - marginTop - marginBottom)) / 2;
		marginRight = marginLeft;

		strokeWidth = marginTop * 1.5f;
		borderWidth = strokeWidth / 5;
		bitmapShield = Bitmap.createBitmap(getMeasuredWidth(),
				getMeasuredHeight(), Bitmap.Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmapShield);

		RectF circleBig = new RectF(marginLeft * 2, marginTop * 2,
				getMeasuredWidth() - 2 * marginRight, getMeasuredHeight() - 2
						* marginBottom);
		RectF circleSmall = new RectF(circleBig);
		float h2 = ((circleBig.bottom - circleBig.top) / 2) / 5.0f;
		resizeCircle(circleSmall, h2);

		drawBorderAndMiddle(canvas, circleBig);
		drawLowZone(canvas, circleBig);
		drawHighZone(canvas, circleBig);
		drawNormalZone(canvas, circleBig);

		float scale = (circleBig.height() / 2) / pointerBitmap.getHeight();
		pointer.setBounds(0, 0, (int) (pointerBitmap.getWidth() * scale),
				(int) (circleBig.height() / 2));
		pointerTX = circleBig.centerX() - pointerBitmap.getWidth() * scale / 2;
		pointerTY = circleBig.top;
		
		textPaint = new Paint();
		textPaint.setColor(Color.GRAY);
		textPaint.setTextSize(getWidth() / 10);
		textPaint.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "samplefont.ttf"));
	}

	/**
	 * Zmiana rozmiaru okręgu.
	 * 
	 * @param circle
	 *            okrąg
	 * @param value
	 *            wartość, o którą jest zmniejszany okrąg
	 */
	private void resizeCircle(final RectF circle, final float value) {
		circle.top += value;
		circle.bottom -= value;
		circle.left += value;
		circle.right -= value;
	}

	/**
	 * Rysuje obramowanie półokręgu i punkt zamocowania wskazówki.
	 * 
	 * @param canvas
	 *            kanwa
	 * @param circleB
	 *            współrzędne większego okręgu
	 */
	private void drawBorderAndMiddle(final Canvas canvas, final RectF circleB) {
		// Czarny łuk szerszy od paska na tarczy wskaźnika
		Path circleStripe = new Path();
		circleStripe.addArc(circleB, Utils.DEGREE_135, Utils.DEGREE_270);
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(borderColor);
		paint.setStyle(Style.STROKE);
		paint.setStrokeCap(Paint.Cap.ROUND);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeWidth(strokeWidth);
		canvas.drawPath(circleStripe, paint);
		
		// Czarne kółko - punkt zamocowania wskazówki.
		RectF oval = new RectF();
		float border = borderWidth * 1.5f;
		oval.left = circleB.centerX() - border;
		oval.right = circleB.centerX() + border;
		oval.top = circleB.centerY() - border;
		oval.bottom = circleB.centerY() + border;
		
		paint.setStyle(Style.FILL);
		canvas.drawOval(oval, paint);
	}

	private void drawLowZone(final Canvas canvas, final RectF circle) {
		//Łuk obszaru niskich wartości.
		Paint bgPaint = new Paint();
		bgPaint.setColor(lzc);
		bgPaint.setAntiAlias(true);
		bgPaint.setStyle(Style.STROKE);
		bgPaint.setStrokeWidth(strokeWidth - borderWidth * 2);
		bgPaint.setStrokeCap(Cap.ROUND);
		bgPaint.setStrokeJoin(Paint.Join.ROUND);
		
		float llAngle = calculateDegreeByValue(ll);
		Path circleStripe = new Path();
		circleStripe.addArc(circle, Utils.DEGREE_135, llAngle);
		
		canvas.drawPath(circleStripe, bgPaint);
	}

	private void drawHighZone(final Canvas canvas, final RectF circle) {
		//Łuk obszaru wysokich wartości.
		Paint bgPaint = new Paint();
		bgPaint.setColor(hzc);
		bgPaint.setAntiAlias(true);
		bgPaint.setStyle(Style.STROKE);
		bgPaint.setStrokeWidth(strokeWidth - borderWidth * 2);
		bgPaint.setStrokeCap(Cap.ROUND);
		bgPaint.setStrokeJoin(Paint.Join.ROUND);
		
		float hiAngle = calculateDegreeByValue(hl);
		Path circleStripe = new Path();
		circleStripe.addArc(circle, (Utils.DEGREE_135 + hiAngle), Math.abs(Utils.DEGREE_360 - (Utils.DEGREE_135 + hiAngle)) + Utils.DEGREE_45);
		
		canvas.drawPath(circleStripe, bgPaint);
	}

	private void drawNormalZone(final Canvas canvas, final RectF circle) {

		//Łuk obszaru średnich wartości.
		float llAngle = calculateDegreeByValue(ll);
		float hlAngle = calculateDegreeByValue(hl);
		
		Path circleStripe = new Path();
		circleStripe.addArc(circle, Utils.DEGREE_135 + llAngle, hlAngle - llAngle);
		
		Paint bgPaint = new Paint();
		bgPaint.setColor(nzc);
		bgPaint.setAntiAlias(true);
		bgPaint.setStyle(Style.STROKE);
		bgPaint.setStrokeWidth(strokeWidth - borderWidth * 2);
		
		if(ll == min || hl == max)
		{
			bgPaint.setStrokeCap(Cap.ROUND);
			bgPaint.setStrokeJoin(Paint.Join.ROUND);
		}
		
		canvas.drawPath(circleStripe, bgPaint);
	}

	/**
	 * 
	 * @param value
	 *            wartość do przedstawienia na wskaźniku.
	 * @return kąt wychylenia reprezentujący podaną wartość na skali.
	 */
	private float calculateDegreeByValue(final float value) {
		return ((value - min) / (max - min)) * Utils.DEGREE_270;
	}

	
	/**
	 * mamy wyliczoną nową wartość prędkości.
	 * 
	 * @param xVelocity
	 *            prędkość w kierunku X
	 * @param yVelocity
	 *            prędkość w kierunku Y
	 */
	private void onVelocityComputed(float xVelocity, float yVelocity) {
		Log.d(TAG, "onVelocityComputed(); xVelocity: " + xVelocity
				+ "; yVelocity: " + yVelocity);
		setValue(xVelocity);
	}

	public void setValue(float val) {

		if (val < min) {
			val = min;
		} else if (val > max) {
			val = max;
		}
		this.value = val;
		this.indicatorLevel = Utils.normalize(value, min, max);
		pointer.setLevel(indicatorLevel);
		invalidate();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (bitmapShield != null) {
			canvas.drawBitmap(bitmapShield, 0, 0, null);
		}
		
		String strVal = String.format(Locale.US, "%07.2f", value);
		canvas.drawText(strVal, getWidth() / 2 - 2 * textPaint.getTextSize(), getHeight() - textPaint.getTextSize(), textPaint);
		
		canvas.save();
		canvas.translate(pointerTX, pointerTY);
		pointer.draw(canvas);
		canvas.restore();
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event)
	{
		vt.addMovement(event);
		vt.computeCurrentVelocity(1000, 3000);
		onVelocityComputed(vt.getXVelocity(), vt.getYVelocity());
		return true;
	}

	@Override
	protected void onAttachedToWindow()
	{
		super.onAttachedToWindow();
		Log.d(TAG, "onAttachedToWindow()");
		vt = VelocityTracker.obtain();
		setOnTouchListener(this);
	}
	
	@Override
	protected void onDetachedFromWindow()
	{
		super.onDetachedFromWindow();
		Log.d(TAG, "onDetachedFromWindow()");
		vt.recycle();
	}
}
